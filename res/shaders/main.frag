#version 330

in vec2 pass_TexCoord;

//-1 no color mod
// 0 color blend
// 1 color only
uniform int color_mode = -1;
uniform sampler2D tex;
uniform vec4 color;

out vec4 out_color;

void main(){
    if(color_mode == 1){
        out_color = color;
    }else{
        vec4 sample_color = texture(tex, pass_TexCoord);

        if(color_mode == -1){
            out_color = sample_color; 
        }else{
            out_color = sample_color * color;
        }
    }
}
