#version 330

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_texCoord;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

uniform int sub_texture = 0;
uniform vec2 tex_size;
uniform vec2 sub_size;
uniform int tex_id;

uniform int flip_x = 0;
uniform int flip_y = 0;

out vec2 pass_TexCoord;
out vec3 pass_Normal;

void main(){

  vec2 mod_texcoord = in_texCoord;
  
  if(flip_x == 1){
    mod_texcoord.x = 1 - mod_texcoord.x;
  }

  if(flip_y == 1){
    mod_texcoord.y = 1 - mod_texcoord.y;
  }
 
  if(sub_texture == 1){
    int per_width = int(tex_size.x / sub_size.x);
    vec2 offset = vec2(sub_size.x * (tex_id % per_width), sub_size.y * (tex_id / per_width));
    mod_texcoord = (offset / tex_size) + (sub_size / tex_size) * mod_texcoord;
  }else{
    mod_texcoord = in_texCoord;
  }
 
  gl_Position = projection * view * model * vec4(in_position, 1.0f);
  pass_TexCoord = mod_texcoord;
}
