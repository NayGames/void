#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <time.h>
#include <unistd.h>
// #include "audio.h"
#include "sys.h"

#include "types.h"
#include "tmath.h"

#define DEBUG_OUTPUT

#ifndef DEBUG_OUTPUT
#undef  _WIN32_WINNT
#define _WIN32_WINNT 0x0500
#endif

#include "input.h"

#include "render.h"

typedef struct timespec tms;

int targetFPS = 60;
int maxTargetFPS = 60;

static r_window window;

static r_shader  default_shader;
static r_camera  default_cam;

// a_buffer test_sound;
// a_music* music;

static mat4 model;

static r_tex sheet_tex;
static r_texsheet sheet;
static r_quad default_quad;

static int tex_id = 0;
static r_baked_layer baked_layer;

int fsign(float x){
    return (int)((x > 0) - (x < 0));
}

void input(double delta){
    if(i_mouse_clicked(0)){
        i_set_mouse_grab(true);
    }

    if(i_key_clicked(GLFW_KEY_ESCAPE)){
        if(!i_get_mouse_grab()){
            r_request_close();
        }

        i_set_mouse_grab(false);
    }

    if(i_get_mouse_grab()){
        float l = i_binding_val("left");
        float r = i_binding_val("right");
        float u = i_binding_val("up");
        float d = i_binding_val("down");
        bool change = false;

        float vert = l + -r;
        float hori = d + -u;

        if(vert != 0){
            default_cam.pos.x += vert * delta * 0.005f;
            change = true;
        }

        if(hori != 0){
            default_cam.pos.y += hori * delta * 0.005f;
            change = true;
        }

        if(change){
            r_update_camera(&default_cam);
        }
    }
}

void init_render(){
    default_shader = r_create_shader("res/shaders/main.vert", "res/shaders/main.frag");

    default_quad = r_create_quad(1.f, 1.f);

    default_cam.pos = (vec3){ 0.0f, 0.0f, -10.f};
    default_cam.size = (vec2){ 16.f, 9.f };
    r_create_camera(&default_cam);

    sheet_tex = r_load_tex("res/textures/texturesheet.png");
    sheet = r_create_tex_sheet(&sheet_tex, 16, 16);

    mat4_identity(&model);
    mat4_translate(&model, 0.f, 0.f ,0.f);

    r_layer layer = r_create_layer(16, 16, &sheet, (vec2){1.f, 1.f}, (vec2){0.f});

    for(int i=0;i<16*16;++i){
        int x = i % layer.width;
        int y = i / layer.width;
        r_set_in_layer(&layer, x, y, 2);
    }

    r_set_in_layer(&layer, 0, 0, 3);
    for(int i=0;i<16;++i){
        int x = i % 4;
        int y = i / 4;
        r_set_in_layer(&layer, x, y, 5);
    }

    baked_layer = r_bake_layer(&layer);
    r_update_camera(&default_cam);
}

// void init_audio(){
//     if(!a_load_devices()){
//         sys_err("Unable to populate audio devices\n");
//         return;
//     }
//
//     if(!a_create_context(NULL)){
//         sys_err("Unable to create audio context.\n");
//         return;
//     }
//
//     music = a_create_music("res/audio/bg.ogg");
//     music->loop = true;
// }

// void exit_audio(){
// #ifdef CACHE_AUDIO_FILES
//     a_destroy_file_cache();
// #endif
//     a_destroy_context();
// }

void update(long delta){
    // a_update_sfx();
    // r_update_camera(&default_cam);
}

void render(){
    if(r_scale_flag){
      r_calculate_scale(default_cam);
    }

    r_bind_shader(&default_shader);
    r_set_uniformi(default_shader, "sub_texture", 1);

    r_set_mat4(default_shader, "projection", default_cam.proj);
    r_set_mat4(default_shader, "view", default_cam.view);
    r_set_mat4(default_shader, "model", model);

    r_set_uniformi(default_shader, "color_mode", -1);

    r_set_vec2(default_shader, "sub_size", (vec2){16.f, 16.f});
    r_set_vec2(default_shader, "tex_size", (vec2){sheet_tex.width, sheet_tex.height});
    r_set_uniformi(default_shader, "tex_id", tex_id);

    r_bind_tex(sheet_tex);
    r_draw_quad(&default_quad);

    r_set_uniformi(default_shader, "sub_texture", 0);
    r_draw_baked_layer(&baked_layer);
}

int main(int argc, char** argv){
#ifdef __MINGW32__
#ifndef DEBUG_OUTPUT
    FreeConsole();
#endif
#endif
    double timeframe = MS_PER_SEC / (double)targetFPS;
    double current = time_get_time();
    double last = current;
    double check;

    double delta;
    double accum = timeframe;

    r_window_info window_info = (r_window_info){
        1280, 720,
        false, false, false,
        60,
        "null"
    };

    r_create_window(window_info);

    init_render();
    // init_audio();

    while(!r_window_should_close()){
        last = current;
        current = time_get_time();
        delta = current - last;
        accum = timeframe;

        r_clear_window();
        render();
        r_swap_buffers();

        i_update();
        glfwPollEvents();
        input(delta);

        update(delta);

        check = time_get_time();
        accum = (long)(check - current);

        double nTimeFrame = timeframe;
        int tFPS;
        int lastFPS = targetFPS;

        if(accum > 0){
            nTimeFrame -= accum;
            tFPS = (int)((double)MS_PER_SEC / (nTimeFrame));

            if(tFPS > maxTargetFPS){
                targetFPS = maxTargetFPS;
            }else if(tFPS > 0){
                targetFPS = tFPS;
            }

            timeframe = (double)(MS_PER_SEC / (double)(targetFPS));

            tms sleepreq, sleeprem;
            sleepreq.tv_sec = 0;
            sleepreq.tv_nsec = accum * NS_PER_MS;

            nanosleep(&sleepreq, &sleeprem);
        }else{
            nTimeFrame += accum;

            tFPS = (int)((double)MS_PER_SEC / (nTimeFrame));

            if(tFPS < maxTargetFPS){
                targetFPS = maxTargetFPS;
            }else if(tFPS < 0){
                targetFPS = 1;
            }else{
                targetFPS = tFPS;
            }

            timeframe = (double)(MS_PER_SEC / (double)(targetFPS));
        }
    }

    // exit_audio();

    r_destroy_shader(&default_shader);
    r_destroy_baked_layer(&baked_layer);
    r_destroy_tex(&sheet_tex);
    r_destroy_quad(&default_quad);
    r_destroy_window(&window);

    glfwTerminate();
    return EXIT_SUCCESS;
}
