#ifndef r_H
#define r_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <stdlib.h>
#include <stdio.h>

#include "tmath.h"
#include "input.h"

#ifndef STD_BOOL_H
#define STD_BOOL_H
typedef enum{false=0,true=1} bool;
#endif

typedef struct r_window_info {
    int width, height;
    bool fullscreen, vsync, borderless;
    int refreshRate;
    char* title;
} r_window_info;

typedef struct r_window {
    int width, height;
    int x, y;
    bool fullscreen, vsync;
    bool close_requested;
    int refreshRate;
    GLFWwindow* glfw;
} r_window;

r_window g_window;

typedef struct r_quad {
    unsigned int vao;
    unsigned int vbo, vto, vboi;
    float width, height;
} r_quad;

typedef struct r_camera {
    vec3 pos, rot;
    mat4 view, proj;
    vec2 size;
    float fov;
    float near;
    float far;
} r_camera;

typedef struct r_shader {
    unsigned int id;
} r_shader;

typedef struct r_tex {
    unsigned int id;
    unsigned int width, height;
} r_tex;

typedef struct r_texsheet {
    r_tex* tex;
    unsigned int subwidth, subheight;
} r_texsheet;

typedef struct {
    r_texsheet* tex;
    unsigned int width, height;
    int* points;
    vec2 offset, tile_size;
} r_layer;

typedef struct {
    unsigned int vao;
    unsigned int vbo, vboi, vto;
    r_texsheet* tex;
    vec2 offset;
    mat4 model;
    unsigned int indc;
} r_baked_layer;

#define TEX_MAPPING_LEN 8
#define TEX_MAPPING_EXPANSION_RATE 32

typedef struct r_texmapping {
    char name[TEX_MAPPING_LEN];
    r_tex* texture;
} r_texmapping;

#define R_ANIMATION_NAME_SIZE 8

typedef struct r_anim {
    unsigned int id;
    char name[R_ANIMATION_NAME_SIZE];
    int* frames;
    int frame_rate;
    int frame_count;
} r_anim;

typedef struct r_anim_viewer {
    r_anim* animation;
    int current_frame;
    int start_frame;
    int end_frame;
    bool loop;
    double time;
} r_anim_viewer;

typedef struct r_sprite {
    r_anim_viewer* anims;
    int animation_count;
    vec3 pos;
    vec2 size;
} r_sprite;

typedef struct r_sprite_recipe {
    r_anim* animations;
    int animation_count;
    vec2 size;
} r_sprite_recipe;

static r_quad quad;

static r_texmapping* tex_mappings;
static int tex_mapping_count = 0;
static int tex_mapping_capacity = 0;

static bool r_init_tex_mappings(int length);
static void r_rm_tex_mapping(char* name);
static void r_rm_tex_mappingi(int index);

bool r_scale_flag;

void            r_calculate_scale(r_camera cam);

r_layer         r_create_layer(int width, int height, r_texsheet* tex, vec2 tile_size, vec2 offset);
r_baked_layer   r_bake_layer(r_layer* layer);
void            r_destroy_layer(r_layer* layer);
void            r_destroy_baked_layer(r_baked_layer* layer);

void r_set_in_layer(r_layer* layer, int x, int y, int id);

r_anim          r_create_animation(r_texsheet* sheet, int* frames, int frame_count, int frame_rate,char* name);
r_anim_viewer   r_view_animation(r_anim* animation, int start_frame, int end_frame);

r_sprite        r_create_sprite(r_sprite_recipe* recipe);
r_sprite_recipe r_create_sprite_recipe(r_anim* animations, int animation_count, vec2 size);

void r_create_camera(r_camera* cam);
void r_update_camera(r_camera* cam);
void r_update_camera_projection(r_camera* cam);

r_tex*      r_get_tex(char* name);
r_tex       r_load_tex(char* path);
void        r_bind_tex(r_tex tex);
void        r_destroy_tex(r_tex* tex);
static void r_assign_tex(r_tex* texture, char name[TEX_MAPPING_LEN]);

r_texsheet r_create_tex_sheet(r_tex* tex, unsigned int subwidth, unsigned int subheight);

void r_get_sub_texcoords(r_texsheet* sheet, unsigned int id, float* coords);

r_quad r_create_quad(float width, float height);
void r_destroy_quad(r_quad* quad);

void r_draw_quad(r_quad* quad);
void r_draw_baked_layer(r_baked_layer* layer);

static GLuint r_get_sub_shader(const char* filePath, int type);
r_shader      r_create_shader(const char* vert, const char* frag);
// r_shader      r_create_shader_f(g_shader_pair pair);
void          r_assign_shader(r_shader* shader, char* name);
void          r_bind_shader(r_shader* shader);
void          r_destroy_shader(r_shader* shader);

int           r_get_uniform_loc(r_shader shader, const char* name);

int r_hex_number(char v);
int r_hex_multi(char* v, int len);
vec3 r_get_color(char* v);

void r_set_uniformf(r_shader shader, const char* name, float value);
void r_set_uniformi(r_shader shader, const char* name, int value);
void r_set_vec4(r_shader shader, const char* name, vec4 value);
void r_set_vec3(r_shader shader, const char* name, vec3 value);
void r_set_vec2(r_shader shader, const char* name, vec2 value);
void r_set_quat(r_shader shader, const char* name, quat value);
void r_set_mat4(r_shader shader, const char* name, mat4 value);

static void r_create_modes();
static bool r_window_info_valid(r_window_info info);
static const GLFWvidmode* r_find_closest_mode(r_window_info info);
static const GLFWvidmode* r_find_best_mode();

static void r_window_resized();

static void glfw_err_cb(int error, const char* msg);
static void glfw_window_pos_cb(GLFWwindow* window, int x, int y);
static void glfw_window_size_cb(GLFWwindow* window, int w, int h);
static void glfw_window_close_cb(GLFWwindow* window);

static void glfw_key_cb(GLFWwindow* window, int key, int scancode, int action, int mods);
static void glfw_mouse_pos_cb(GLFWwindow* window, double x, double y);
static void glfw_mouse_button_cb(GLFWwindow* window, int button, int action, int mods);
static void glfw_scroll_cb(GLFWwindow* window, double dx, double dy);
static void glfw_joy_cb(int joystick, int action);
static void glfw_char_cb(GLFWwindow* window, unsigned int c);

bool r_create_window(r_window_info info);
void r_destroy_window();
void r_request_close();

void r_center_window();
void r_set_window_pos(int x, int y);
bool r_no_err();
void r_loop_err();

bool r_window_should_close();

void r_swap_buffers();
void r_clear_window();
void r_poll_window();
#endif
