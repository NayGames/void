<div id="header">
    <h3 id="title">void</h3>
    <img width="64px" height="64px" src="res/textures/skeleton_large.png"/>
    <p id="subtitle">Made by <a href="http://tek256.com">tek</a> with love.</p>
</div>

#### about
void is a cross platform 2d adventure game.  
#### build
you can find build instructions in the docs/build.md file!
