**linux**  
You'll want to clone the repository and move into it.  
`git clone https://gitlab.com/tek256/void && cd void`  

It's in your best interest to create a build folder as well.  
`mkdir build && cd build`  

From here you'll want to generate the makefiles.  
`cmake ../ -G "Unix Makefiles"`  

Once you have the makefiles generated, you'll want to build them.  
`make`  

Now you should be able to run void  
`./void`  

**windows**  
_Note: All of this can be done in powershell or cmd._
You'll want to clone the repository and move into it.  
`git clone https://gitlab.com/tek256/void`   
`cd void`   

It's in your best interest to create a build folder as well.  
`mkdir build`    
`cd build`

From here you'll want to generate the makefiles.  
`cmake ../ -G "MinGW Makefiles"`  

Once you have the makefiles generated, you'll want to build them.  
`mingw32-make`  

Now you should be able to run void  
`./void.exe`  
